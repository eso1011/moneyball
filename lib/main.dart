import 'package:flutter/material.dart';
import 'package:money_ball/constants.dart';
import 'package:money_ball/screens/categories/categories_screen.dart';
import 'package:money_ball/screens/categories/create_category_screen.dart';

void main() => runApp(MoneyBallApp());

class MoneyBallApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/categories',
      title: 'Money Ball',
      theme: ThemeData(
          primaryColor: kPrimaryColor,
          accentColor: kPrimaryColor,
          textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor)),
      routes: {
        '/categories': (context) => CategoriesScreen(),
        '/categories/new':  (context) => CreateCategoryScreen(),
      },
    );
  }
}
