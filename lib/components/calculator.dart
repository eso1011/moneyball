import 'package:flutter/material.dart';
import 'package:flutter_simple_calculator/flutter_simple_calculator.dart';

import '../constants.dart';


class Calculator extends StatelessWidget {
  final Function onChanged;
  final String title;
  final Color color;


  const Calculator({
    Key key,
    @required this.onChanged, this.title, this.color,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(title, style: TextStyle(fontSize: kDefaultFontSize*3, color: color),),
        Flexible(
          child: SimpleCalculator(
            value: 0,
            hideExpression: true,
            onChanged: onChanged,
            theme: CalculatorThemeData(
                operatorColor: color
            ),
          ),
        ),
      ],
    );
  }
}
