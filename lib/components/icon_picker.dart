import 'package:flutter/material.dart';

import '../constants.dart';

class IconPicker extends StatefulWidget {
  final Function onIconChanged;
  final IconData chosenIcon;
  static List<IconData> icons = [
    Icons.ac_unit,
    Icons.access_alarm,
    Icons.access_time,
    Icons.home,
    Icons.attach_money,
    Icons.local_grocery_store,
    Icons.android,
    // all the icons you want to include
  ];

  const IconPicker({
    Key key,
    @required this.onIconChanged,
    @required this.chosenIcon,
  }) : super(key: key);

  @override
  _IconPickerState createState() => _IconPickerState();
}

class _IconPickerState extends State<IconPicker> {
  IconData _chosenIcon;

  @override
  void initState() {
    _chosenIcon = widget.chosenIcon;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.extent(
      maxCrossAxisExtent: 60,
      crossAxisSpacing: 8,
      mainAxisSpacing: 8,
      padding: const EdgeInsets.all(16.0),
      children: <Widget>[
        for (var icon in IconPicker.icons)
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: _chosenIcon == icon ? Colors.black : Colors.black38,
            ),
            child: IconButton(
              onPressed: () {
                widget.onIconChanged(icon);
                setState(() {
                  _chosenIcon = icon;
                });
              },
              icon: Icon(icon),
              color: Colors.white,
            ),
          )
      ],
    );
  }
}
