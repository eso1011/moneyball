import 'package:flutter/material.dart';

import '../constants.dart';

class NavbarDrawer extends StatelessWidget {
  const NavbarDrawer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Навигационная панель'),
            decoration: BoxDecoration(
              color: kPrimaryColor,
            ),
          ),
          ListTile(
            title: Text('Экран Категории'),
            onTap: () {
              Navigator.pop(context);
              // просто закрываем если идем на главный экран
              // Navigator.pushNamed(context, '/categories');
            },
          ),
          ListTile(
            title: Text('Создать категорию'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushNamed(context, '/categories/new');
            },
          ),
        ],
      ),
    );
  }
}
