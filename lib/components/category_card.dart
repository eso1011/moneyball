import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:money_ball/components/calculator.dart';
import 'package:money_ball/constants.dart';

class CategoryCard extends StatelessWidget {
  final String name;
  final Color color;
  final IconData icon;
  final DocumentReference reference;

  const CategoryCard({
    Key key,
    this.name,
    this.color,
    this.icon,
    this.reference,
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {

    void _createTransaction(key, value, expression) {
      if (key == "=") {
        final database = Firestore.instance;
        database.collection('transactions').add({
          "value": value,
          "category": reference,
          "created_at": DateTime.now()
        });
        Navigator.of(context).pop();
      }
    }

    return Column(children: <Widget>[
      Container(
        margin: EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: Icon(icon),
              color: Colors.white,
              onPressed: () {
                showModalBottomSheet(
                    isScrollControlled: true,
                    context: context,
                    builder: (BuildContext context) {
                      return SizedBox(
                        height: MediaQuery.of(context).size.height * 0.75,
                        child: Calculator(
                          onChanged: _createTransaction,
                          title: name,
                          color: color,
                        ),
                      );
                    });
              },
            )
          ],
        ),
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
      ),
      RichText(
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.center,
        text: TextSpan(style: TextStyle(color: kTextColor), text: name),
      ),
    ]);
  }
}
