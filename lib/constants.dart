import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF0C9869);
const kTextColor = Color(0xFF3C4046);
const kBackgroundColor = Color(0xFFF9f8fd);

const double kDefaultPadding = 20.0;
const double kDefaultFontSize = 8.0;