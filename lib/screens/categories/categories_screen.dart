import 'package:flutter/material.dart';
import 'package:money_ball/components/category_card.dart';
import 'package:money_ball/components/navbar_drawer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CategoriesScreen extends StatefulWidget {
  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Categories"),
      ),
      drawer: NavbarDrawer(),
      body: Container(
        margin: EdgeInsets.all(8),
        // decoration: BoxDecoration(color: Colors.red),
        child: StreamBuilder(
          stream: Firestore.instance.collection('categories').snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return const Text("Loading...");
            return GridView.builder(
              itemBuilder: (context, index) =>
                  _buildListItem(context, snapshot.data.documents[index]),
              itemCount: snapshot.data.documents.length,
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 100),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/categories/new');
        },
        tooltip: 'Add category',
        child: Icon(Icons.add),
      ), // This ,
    );
  }

  _buildListItem(BuildContext context, DocumentSnapshot category) {
    return CategoryCard(
      name: category["name"],
      icon: IconData(category["icon_id"], fontFamily: 'MaterialIcons'),
      color: Color(category["color"]),
      reference: category.reference,
    );
  }
}
