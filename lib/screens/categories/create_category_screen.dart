import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:money_ball/components/icon_picker.dart';

class CreateCategoryScreen extends StatefulWidget {
  @override
  _CreateCategoryScreenState createState() => _CreateCategoryScreenState();
}

class _CreateCategoryScreenState extends State<CreateCategoryScreen> {
  IconData _icon = Icons.android;
  ColorSwatch _tempMainColor;
  Color _tempShadeColor = Colors.blue[800];
  ColorSwatch _mainColor = Colors.blue;
  Color _shadeColor = Colors.blue[800];
  final categoryNameController = TextEditingController();
  bool _showAddButton = false;

  void _openDialog(String title, Widget content) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(1.0),
          insetPadding: const EdgeInsets.all(10),
          content: content,
          actions: [
            FlatButton(
              child: Text('CANCEL'),
              onPressed: Navigator.of(context).pop,
            ),
            FlatButton(
              child: Text('DONE'),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() => _mainColor = _tempMainColor);
                setState(() => _shadeColor = _tempShadeColor);
              },
            ),
          ],
        );
      },
    );
  }

  void _openIconPopup() async {
    _openDialog(
      "Color picker",
      Container(
        width: MediaQuery.of(context).size.width,
        height: 500,
        child: DefaultTabController(
            length: 2,
            child: Scaffold(
              appBar: AppBar(
                bottom: TabBar(
                  tabs: <Widget>[
                    Tab(
                      icon: Icon(Icons.color_lens),
                      text: 'Color',
                    ),
                    Tab(
                      icon: Icon(Icons.local_grocery_store),
                      text: 'Icon',
                    )
                  ],
                ),
                title: Text("Icon customization"),
                automaticallyImplyLeading: false,
              ),
              body: TabBarView(
                children: <Widget>[
                  MaterialColorPicker(
                    selectedColor: _shadeColor,
                    onColorChange: (color) =>
                        setState(() => _tempShadeColor = color),
                    onMainColorChange: (color) =>
                        setState(() => _tempMainColor = color),
                    onBack: () => print("Back button pressed"),
                  ),
                  IconPicker(
                    onIconChanged: (icon) {
                      setState(() {
                        _icon = icon;
                      });
                    },
                    chosenIcon: _icon,
                  ),
                ],
              ),
            )),
      ),
    );
  }

  void _addCategory() {
    final database = Firestore.instance;
    database.collection('categories').add({
      "name": categoryNameController.text,
      "color": _shadeColor.value,
      "icon_id": _icon.codePoint,
    });
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create category'),
      ),
      floatingActionButton: (_showAddButton)
          ? FloatingActionButton(
              onPressed: _addCategory,
              tooltip: "Save new category",
              child: Icon(Icons.check),
            )
          : null,
      body: Container(
        margin: EdgeInsets.all(8),
        child: Column(
          children: [
            Row(
              children: <Widget>[
                Container(
                  child: IconButton(
                    onPressed: _openIconPopup,
                    icon: Icon(_icon),
                    color: Colors.white,
                  ),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _shadeColor,
                  ),
                ),
                Flexible(
                  child: Container(
                    margin: EdgeInsets.only(left: 16),
                    child: TextField(
                      controller: categoryNameController,
                      onChanged: (text) {
                          setState(() {
                            _showAddButton = text.isNotEmpty;
                          });
                      },
                      autofocus: true,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Input category name',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
