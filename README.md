# money_ball

App for money management

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


# Инструкция по развертыванию
1) Установить флаттер в систему по инструкции https://flutter.dev/docs/get-started/install;
2) Скачать Android-studio;
3) Установить пакеты Dart и Flutter - рестартнуть;
4) После чего подключить устройство к компьютеру или установить виртуальное Tools > AVD Manager;
5) Выбрать устройство и нажать Run. Приложение откроется в debug-моде;
